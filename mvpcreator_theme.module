<?php
/**
 * @file
 * Code for the MVPCreator Theme feature.
 */

/**
 * Implements of hook_ctools_plugin_directory()
 */
function mvpcreator_theme_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/' . $plugin_type;
  }
  elseif ($owner == 'panels' && $plugin_type == 'layouts') {
    return 'plugins/' . $plugin_type;
  }
  elseif ($owner == 'panels' && $plugin_type == 'styles') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Get the Panel variant used to generate the current page.
 */
function mvpcreator_theme_current_panels_variant() {
  $variant =& drupal_static(__FUNCTION__, NULL);

  if (is_null($variant)) {
    $page = page_manager_get_current_page();

    if (empty($page)) {
      $variant = FALSE;
    }
    elseif ($page['handler']->handler == 'panelizer_node') {
      switch ($page['task']['name']) {
        case 'term_view':
          $displayed_entity = taxonomy_term_load($page['arguments'][0]);
          break;

        case 'user_view':
          $displayed_entity = user_load($page['arguments'][0]);
          break;

        case 'comment_view':
          $displayed_entity = comment_load($page['arguments'][0]);
          break;

        default:
          $displayed_entity = node_load($page['arguments'][0]);

          // If using workbench moderation, and this is a node draft, then
          // load the draft revision (rather than the latest revision).
          if (module_exists('workbench_moderation') && arg(0) == 'node' && arg(2) == 'draft') {
            $displayed_entity = workbench_moderation_node_current_load($displayed_entity);
          }
      }
      if (!empty($displayed_entity->panelizer['page_manager']->display)) {
        $variant = $displayed_entity->panelizer['page_manager']->display;
      }
    }
    elseif ($page['handler']->handler == 'panel_context') {
      $variant = $page['handler']->conf['display'];
    }
    else {
      $variant = FALSE;
    }
  }

  return $variant;
}

/**
 * Determine if the current page is full width or not.
 */
function mvpcreator_theme_is_page_full_width() {
  $full_width =& drupal_static(__FUNCTION__, NULL);

  if (is_null($full_width)) {
    if (!theme_get_setting('full_width')) {
      $full_width = FALSE;
    }
    elseif (strpos($_GET['q'], 'panels/ajax/ipe/save_form/') === 0) {
      // We make the (dangerous) assumption that if the the theme supports
      // 'full width' and we're saving the layout with the IPE that we can
      // render this as 'full width'.
      $full_width = TRUE;
    }
    elseif ($display = mvpcreator_theme_current_panels_variant()) {
      $layout = panels_get_layout($display->layout);
      if (!empty($layout['full width'])) {
        $full_width = (bool)$layout['full width'];
      }
    }
  }

  return $full_width;
}

/**
 * Determine if a region in a layout is full width or not.
 *
 * @param string $region
 *   Region machine name.
 * @param string $layout
 *   (Optional) Layout machine name.
 *
 * @return bool
 *   TRUE if the region is full width; otherwise FALSE.
 */
function mvpcreator_theme_is_region_full_width($region, $layout = '') {
  if (empty($layout)) {
    if ($display = mvpcreator_theme_current_panels_variant()) {
      $layout = $display->layout;
    }
    else {
      return FALSE;
    }
  }

  if ($layout_info = panels_get_layout($layout)) {
    return !empty($layout_info['full width']) && is_array($layout_info['full width regions']) && in_array($region, $layout_info['full width regions']);
  }

  return FALSE;
}

/**
 * Determines if the passed display should be rendered full width or not.
 *
 * @param object $display
 *   A Panels display object.
 *
 * @return bool
 *   TRUE if the passed display should be rendered full width; otherwise FALSE.
 */
function mvpcreator_theme_is_display_full_width($display) {
  if (mvpcreator_theme_is_page_full_width()) {
    if ($page_variant = mvpcreator_theme_current_panels_variant()) {
      return $page_variant->did === $display->did;
    }
    else {
      // Special case for saving in the IPE.
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Implements hook_theme_registry_alter().
 */
function mvpcreator_theme_theme_registry_alter(&$registry) {
  ctools_include('plugins', 'panels');

  // Add a preprocess function to every layout plugin theme function.
  $layouts = panels_get_layouts();
  foreach ($layouts as $name => $data) {
    if (!empty($data['full width'])) {
      foreach (array('theme', 'admin theme') as $callback) {
        if (!empty($data[$callback])) {
          $registry[$data[$callback]]['preprocess functions'][] = '_mvpcreator_theme_preprocess_layout';
        }
      }
    }
  }

  // Add a preprocess function to every style plugins render pane function.
  $styles = panels_get_styles();
  foreach ($styles as $name => $data) {
    if (!empty($data['full width']) && !empty($data['render region'])) {
      $registry[$data['render region']]['preprocess functions'][] = '_mvpcreator_theme_preprocess_render_region';
    }
  }

  if (isset($registry['panels_pane'])) {
    if (($index = array_search('template_preprocess_panels_pane', $registry['panels_pane']['preprocess functions'])) !== FALSE) {
      array_splice($registry['panels_pane']['preprocess functions'], $index, 0, 'mvpcreator_theme_prepreprocess_panels_pane');
    }
  }
}

/**
 * Implements hook_theme().
 */
function mvpcreator_theme_theme() {
  ctools_include('plugins', 'panels');

  $theme = array();

  // Register the theme functions for styles which we wrapped, because Panels
  // won't do it for us.
  $styles = panels_get_styles();
  foreach ($styles as $name => $data) {
    if (!empty($data['full width']) && !empty($data['original render region'])) {
      $theme[$data['original render region']] = array(
        'variables' => array('display' => NULL, 'owner_id' => NULL, 'panes' => NULL, 'settings' => NULL, 'region_id' => NULL, 'style' => NULL),
        'path' => $data['path'],
        'file' => $data['file'],
      );

      if (!empty($data['hook theme'])) {
        if (is_array($data['hook theme'])) {
          $theme += $data['hook theme'];
        }
        else if (function_exists($data['hook theme'])) {
          $data['hook theme']($theme, $data);
        }
      }
    }
  }

  return $theme;
}

/**
 * Preprocess function for all layouts.
 */
function _mvpcreator_theme_preprocess_layout(&$vars) {
  if (!empty($vars['layout']['full width'])) {
    $vars['full_width'] = mvpcreator_theme_is_display_full_width($vars['display']);
    $vars['classes_array'][] = 'mvpcreator-theme-full-width';
  }
}

/**
 * Preprocess function for all region style plugins.
 */
function _mvpcreator_theme_preprocess_render_region(&$vars) {
  // Let styles that support 'full width' know if they should render full width!
  if (!empty($vars['style']['full width'])) {
    $vars['full_width'] = mvpcreator_theme_is_display_full_width($vars['display']);
  }
}

/**
 * Implements hook_ctools_plugin_post_alter().
 */
function mvpcreator_theme_ctools_plugin_post_alter(&$plugin, &$info) {
  if ($info['type'] == 'styles') {
    // If region plugin doesn't support 'full width', then we replace it's theme
    // function with a wrapper that does.
    if (!empty($plugin['render region']) && empty($plugin['full width'])) {
      $plugin['full width'] = TRUE;
      $plugin['original render region'] = $plugin['render region'];
      $plugin['render region'] = 'mvpcreator_theme_wrap_render_region';
    }
  }
}

/**
 * Theme function for rendering styles without support for 'full width'.
 */
function theme_mvpcreator_theme_wrap_render_region($vars) {
  $output = '';

  $add_container = $vars['full_width'] && mvpcreator_theme_is_region_full_width($vars['region_id'], $vars['display']->layout);

  if ($add_container) {
    $output .= '<div class="container">';
  }

  $output .= theme($vars['style']['original render region'], $vars);

  if ($add_container) {
    $output .= '</div>';
  }

  return $output;
}

/**
 * Invokes hook on modules and then the current them.
 */
function mvpcreator_theme_invoke_all($hook) {
  global $theme, $base_theme_info;

  $args = func_get_args();

  // First, do all the modules.
  $values = call_user_func_array('module_invoke_all', $args);

  // Then, to the base themes in order and finally the current them.
  $hook = array_shift($args);
  if (isset($theme)) {
    $theme_keys = array();
    foreach ($base_theme_info as $base) {
      $theme_keys[] = $base->name;
    }
    $theme_keys[] = $theme;

    foreach ($theme_keys as $theme_key) {
      $function = $theme_key . '_' . $hook;
      if (function_exists($function)) {
        $result = call_user_func_array($function, $args);
        if (isset($result) && is_array($result)) {
          $values = array_merge_recursive($values, $result);
        }
        elseif (isset($result)) {
          $values[] = $result;
        }
      }
    }
  }

  return $values;
}

/**
 * Implements hook_mvpcreator_theme_mvpcreator_theme_colors().
 */
function mvpcreator_theme_mvpcreator_theme_colors() {
  return array(
    'default' => t('Default color'),
    'custom' => t('Custom color'),
  );
}

/**
 * Implements hook_mvpcreator_theme_mvpcreator_theme_colors_alter().
 */
function mvpcreator_theme_mvpcreator_theme_colors_alter(&$colors) {
  // Move the 'custom' color to the end of the list (unless some other alter
  // removed it).
  if (isset($colors['custom'])) {
    $custom = $colors['custom'];
    unset($colors['custom']);
    $colors['custom'] = $custom;
  }
}

/**
 * Get a list of the available color options.
 *
 * @return array
 *   An associative array with a unique machine name as key and a human
 *   readable label as the value.
 */
function mvpcreator_theme_get_color_options() {
  $colors = mvpcreator_theme_invoke_all('mvpcreator_theme_colors');
  drupal_alter('mvpcreator_theme_colors', $colors);
  return $colors;
}

/**
 * Checks if an HTML color code is valid.
 *
 * @param string $value
 *   A string entered by the user.
 */
function mvpcreator_theme_is_valid_color($value) {
  return preg_match('/^#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/', $value);
}

/**
 * Implements hook_mvpcreator_theme_mvpcreator_theme_background_images().
 */
function mvpcreator_theme_mvpcreator_theme_background_images() {
  return array(
    'none' => t('No image'),
    'custom' => t('Custom image'),
  );
}

/**
 * Implements hook_mvpcreator_theme_mvpcreator_theme_background_images_alter().
 */
function mvpcreator_theme_mvpcreator_theme_background_images_alter(&$images) {
  // Move the 'custom' image to the end of the list (unless some other alter
  // removed it).
  if (isset($images['custom'])) {
    $custom = $images['custom'];
    unset($images['custom']);
    $images['custom'] = $custom;
  }
}

/**
 * Get a list of the available background image options.
 *
 * @return array
 *   An associative array with a unique machine name as key and a human
 *   readable label as the value.
 */
function mvpcreator_theme_get_background_image_options() {
  $images = mvpcreator_theme_invoke_all('mvpcreator_theme_background_images');
  drupal_alter('mvpcreator_theme_background_images', $images);
  return $images;
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function mvpcreator_theme_prepreprocess_panels_pane(&$vars) {
  $content = $vars['content'];
  if (!empty($content->style_attribute)) {
    $vars['attributes_array']['style'] = $content->style_attribute;
  }
}
